#!/usr/bin/env bash

source ci/utils.sh

function create_module_file {
    export software=$1
    export version=$2
    export wrappers_path=${SINGULARITY_REPOSITORY_PATH}/wrappers/${software}/${version}

    j2 ci/singularity/modulefile.j2 tools/${software}/${version}/meta.yml
}

tools_list=$1

if [ -z $tools_list ]; then
  echo "No tools list provided"
  exit 1
fi

# creating envs
for software_line in $(cat $tools_list); do
  software=$(echo $software_line | cut -f1 -d/);
  version=$(echo $software_line | cut -f2 -d/);

  image_path=${SINGULARITY_REPOSITORY_PATH}/images/${software}-${version}.sif
  definition_path=tools/${software}/${version}/image.def

  echo -e "\033[1mDeploying ${software} ${version}\033[0m \xF0\x9F\x9A\x80"

  echo "Building Singularity image..."
  singularity build --force $image_path $definition_path

  echo "Creating wrappers..."
  python3 ci/singularity/build_wrappers.py $software $version

  echo "Creating modulefile..."
  mkdir -p ${MODULEFILES_PATH}/${software}
  create_module_file $software $version > ${MODULEFILES_PATH}/${software}/${version}
done
