#!/usr/bin/env bash

function add_bot_message_to_mr() {
  if [ $CI_MERGE_REQUEST_ID ]
  then
    curl --request POST --header "Authorization: Bearer $BOT_API_TOKEN" --data-urlencode "body=$1"   https://gitlab.com/api/v4/projects/${CI_MERGE_REQUEST_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/notes
  fi
}

function validate_yaml() {
  schema=$1
  yaml=$2

  # yamale: validate yaml file based on schema
  # grep: get "Error validating data" line and all lines after
  # grep: remove "Error validating data" line to get only explicit errors line
  # sed: remove heading and traling spaces
  # awk: remove any blank lines
  yamale -s $schema -n 1 $yaml 2> /dev/null | grep -A 1000 "Error validating data" | grep -v "Error validating data" | sed '/^$/d' | awk '{$1=$1};1'
}

function run() {
  if [ "$REMOTE_MODE" = true ]; then
    ssh ${CLUSTER_USER}@${CLUSTER_HOST} $1
  else
    eval $1
  fi
}
