#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ -z $SOFTWARE ]; then
  if [ "$CI_PIPELINE_SOURCE" == "merge_request_event" ]; then
    START_REF=$(git merge-base origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME HEAD)
    END_REF="HEAD"
  else
    START_REF="${CI_COMMIT_BEFORE_SHA}"
    END_REF="${CI_COMMIT_SHA}"
  fi

  tools=$(git diff --name-only ${START_REF} ${END_REF} | grep -E "^tools/.*$" | grep -v "sample" | sort -u)
else
  # we create a fake modification to keep going with the change check algo
  tools="tools/${SOFTWARE}/${VERSION}/meta.yml"
  if ! [ -f $tools ]; then
    echo -e "\xE2\x9D\x8C${SOFTWARE} ${VERSION} is not available"
    exit 1
  fi
fi

touch ${DIR}/conda-tools.list
touch ${DIR}/singularity-tools.list

> $DIR/checked_tools.list

for tool in $tools
do
  software=$(echo $tool | cut -f2 -d/)
  version=$(echo $tool | cut -f3 -d/)
  if ! grep -q "${software} ${version}" $DIR/checked_tools.list; then
    echo "${software} ${version}" >> $DIR/checked_tools.list



    # determine deployment mode
    meta_file="tools/${software}/${version}/meta.yml"

    if ! [ -f "$meta_file" ]; then
      >&2 echo "No meta file for ${software} ${version}"
      exit 1
    fi

    deployment=$(awk '/deployment/ {print $2}' $meta_file)
    echo "${software}/${version}" >> ${DIR}/${deployment}-tools.list
  fi
done

rm -f $DIR/checked_tools.list
