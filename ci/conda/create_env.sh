#!/usr/bin/env bash

source ci/utils.sh

function create_module_file {
    export software=$1
    export version=$2
    export env_path=${CONDA_HOME}/envs/${software}-${version}
    export env_content=$(run "ls ${env_path}")

    run "mkdir -p ${MODULEFILES_PATH}/${software}"

    if [ "$REMOTE_MODE" = true ]; then
      module_file="/tmp/modulefile"
    else
      module_file="${MODULEFILES_PATH}/${software}/${version}"
    fi

    j2 ci/conda/modulefile.j2 tools/${software}/${version}/meta.yml > ${module_file}
    cat ${module_file}

    if [ "$REMOTE_MODE" = true ]; then
      scp $module_file ${CLUSTER_USER}@${CLUSTER_HOST}:${MODULEFILES_PATH}/${software}/${version}
      rm /tmp/modulefile
    fi

}

CONDA_BIN=$CONDA_HOME/bin/conda
run "${CONDA_BIN} --version"

tools_list=$1

exit_code=0

# creating envs
for software_line in $(cat $tools_list); do

    software=$(echo $software_line | cut -f1 -d/);
    version=$(echo $software_line | cut -f2 -d/);

    env_file="tools/${software}/${version}/env.yml"
    if [ "$REMOTE_MODE" = true ]; then
      env_file_path="/tmp/${CI_JOB_ID}-env_file.yml"
      scp ${env_file} ${CLUSTER_USER}@${CLUSTER_HOST}:$env_file_path
    else
      env_file_path=$env_file
    fi

    echo -e "\033[1mDeploying ${software} ${version}\033[0m \xF0\x9F\x9A\x80"

    echo "Creating conda environment..."
    run "${CONDA_BIN} env create -f ${env_file_path}" 2> stderr
    if [ $? -eq 0 ]; then
        echo "Creating modulefile..."
        create_module_file $software $version 2> /dev/null
    else
        if grep -q "CondaValueError: prefix already exists:" stderr; then
            echo "Environment already exists"
            echo "Updating existing environment..."
            run "${CONDA_BIN} env update -f ${env_file_path}" 2> stderr
            if [ $? -eq 0 ]; then
                echo "Updating modulefile..."
                create_module_file $software $version
            else
                echo -e "\xE2\x9D\x8C Error while updating"
                cat stderr
                exit_code=1
            fi

        else
            echo -e "\xE2\x9D\x8C Error while creating"
            cat stderr
            exit_code=1
        fi
    fi

    if [ "$REMOTE_MODE" = true ]; then
      run "rm $env_file_path"
    fi
done

exit $exit_code
